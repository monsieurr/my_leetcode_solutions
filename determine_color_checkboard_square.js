/**
 * [1812] https://leetcode.com/problems/determine-color-of-a-chessboard-square/
 * You are given coordinates, a string that represents the coordinates of a square of the chessboard. 
 * Below is a chessboard for your reference.
 */

/**
 * @param {string} coordinates
 * @return {boolean}
 */
 var squareIsWhite = function(coordinates) {
    let numberArray = [1, 2, 3, 4, 5, 6, 7, 8]
    let charArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    
    console.log(coordinates)
    console.log('number : ', coordinates.split('')[1])
    console.log('letter : ', coordinates.split('')[0])
    
    let isCharEven = false;
    
    for(let i=0; i < charArray.length; i++) {
        if(charArray[i] === coordinates.split('')[0]) {
            console.log("charArray[i] : ", charArray[i])
            if(i % 2 === 0) {
                console.log("charArray[i] : ", charArray[i])
                isCharEven = true;
            }
            break;
        }
    }
    
    console.log("isCharEven value : ", isCharEven)
 

    if(coordinates.split('')[1] % 2 === 0 && !isCharEven) {
            //this square must be black because even number for both arrays means black
            return false;
    }
    else if(coordinates.split('')[1] % 2 === 0 && isCharEven) {
            //this square must be white because even number for nArray and odd number for cArray
            return true;
    }
    else if(coordinates.split('')[1] % 2 !== 0 && !isCharEven) {
            //this square must be white because odd number for nArray and even number for cArray
            return true;
    }
    else if(coordinates.split('')[1] % 2 !== 0 && isCharEven) {
            //this square must be black because odd number for both arrays means false
            return false;
    } else {
            console.log("we have a problem here, this case wasn't means to exist")
    }

};
