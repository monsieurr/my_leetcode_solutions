# my_leetcode_solutions

Solutions to the leetcode problems I solved, currently doing it in JS but I might switch language at some point just for fun.
I will try to update as I solve them, see my progression here : https://leetcode.com/monsieurr/
