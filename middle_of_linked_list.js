/**
 * https://leetcode.com/problems/middle-of-the-linked-list/
 * [876] Given the head of a singly linked list, return the middle node of the linked list.
 * If there are two middle nodes, return the second middle node.
 */

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var middleNode = function(head) {
    console.log(head);

    console.log(head.next);
    
    let arrList = head;
    
    let count = 0;
    
    while(head !== null) {
        head = head.next; 
        count += 1
    }
    console.log('count : ', count);
    
    let midCount = Math.floor(count / 2);
    console.log('midCount : ', midCount);
    
    for(let i = 0; i < midCount ; i++) {
        console.log('iteration : ', i)
        arrList = arrList.next;
    }
    
    //console.log(head);
    head = arrList;
    console.log('head final : ', head);
    
    return head;

};
