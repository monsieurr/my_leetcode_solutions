/**
 * https://leetcode.com/problems/find-numbers-with-even-number-of-digits/
 * [1295] Given an array nums of integers, return how many of them contain an even number of digits.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */


 var findNumbers = function(nums) {
    let numberOfDigits;
    let evenNumberDigitsCount = 0;
    
    for(let i = 0; i < nums.length; i++) {
        console.log("nums[i]", nums[i])
        console.log("nums[i].length", nums[i].length)
        console.log("evenNumberDigitsCounts", evenNumberDigitsCount)
        if(getlength(nums[i]) % 2 == 0) {
            evenNumberDigitsCount += 1
        }
    }
    
    return evenNumberDigitsCount;
};

function getlength(number) {
    return number.toString().length;
}