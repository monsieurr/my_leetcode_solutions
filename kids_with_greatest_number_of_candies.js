/*
* https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/
* [1431] There are n kids with candies. You are given an integer array candies, where each candies[i] represents the number of candies the ith kid has, and an integer extraCandies, denoting the number of extra candies that you have.
* Return a boolean array result of length n, where result[i] is true if, after giving the ith kid all the extraCandies, they will have the greatest number of candies among all the kids, or false otherwise.
* Note that multiple kids can have the greatest number of candies.
*/

// Final version has the console.log removed but I decided to let them here for "testing" purpose (I know not the best way to test
// but still very efficient for a lone devlopper)

/**
 * @param {number[]} candies
 * @param {number} extraCandies
 * @return {boolean[]}
 */
 var kidsWithCandies = function(candies, extraCandies) {
    let result = new Array(candies.length).fill(1)
    
    console.log('candies : ', candies)
    console.log('extra candies : ', extraCandies)
    
    for(let i=0; i < candies.length; i++) {
        console.log("ITERATION NUMBER : ", i)
        console.log('candies : ', candies)
        console.log(`Kid ${i} has ${candies[i]} candies ---`);
        console.log(`With extra candies Kid ${i} has ${candies[i] + extraCandies} candies`);
        
        let extraCandyKid = candies[i] + extraCandies;
        
        for(let j=0; j < candies.length; j++) {
            if(j == i) {
                console.log(`nothing it's the same kid ${i} I don't care about him`)
            }
            else if(extraCandyKid < candies[j]) {
                console.log("extra candy kid : ", extraCandyKid)
                console.log(`Kid ${j} has ${candies[j]} candies MORE than than extra candy kid---`)
                result[i] = 0
            }
        }

    }
    console.log(candies)
    console.log(extraCandies)
    
    return result
};