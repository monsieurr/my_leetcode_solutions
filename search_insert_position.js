/**
 * https://leetcode.com/problems/search-insert-position/
 * [35] Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
 * You must write an algorithm with O(log n) runtime complexity.
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
 let searchInsert = function(nums, target) {
    //console.log('nums : ', nums);
    //console.log('target : ', target);
    let ind = -1;

    if(target > nums[nums.length - 1]) {
        return nums.length;
    }
    
    if(target < nums[0]) {
        return 0;
    }
    
    for(let i = 0; i < nums.length; i++) {
        //console.log('nums[i] : ', nums[i]);
        if(nums[i] === target) {
            //console.log('inside the loop')
            ind = i;
            break;
        }
    }
    
    //console.log('--- target not found in array - second step ---')
    //console.log('nums : ', nums);
    //console.log('target : ', target);
    
    if(ind === -1) {
        for(let i = 0; i < nums.length; i++) {
            //console.log('nums[i] : ', nums[i]);
            //console.log('i : ',i)
            if(target >= nums[i] && target <= nums[i + 1]) {
                //console.log('inside the loop')
                ind = i + 1;
                break;
            }
    } 
    }
    
    //console.log('index returned : ', ind)
    
    return ind;
};
