/**
 * https://leetcode.com/problems/single-number/
 * [136] Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
 * You must implement a solution with a linear runtime complexity and use only constant extra space.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
 let singleNumber = function(nums) {
    numsSet = [...new Set(nums)];
    uniqNums = Array.from(numsSet);
    count = 0;
    
    for(let i = 0; i < uniqNums.length; i++) {
        for(let j = 0; j < nums.length; j++) {
            if(uniqNums[i] === nums[j]) {
                count += 1;
            }
        }
        
        if(count === 1) {
            return uniqNums[i];
        }
        
        count = 0;
    }
};
