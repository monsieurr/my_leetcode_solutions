/**
 * https://leetcode.com/problems/self-dividing-numbers/
 * [728] A self-dividing number is a number that is divisible by every digit it contains.
 * For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.
 * A self-dividing number is not allowed to contain the digit zero.
 * Given two integers left and right, return a list of all the self-dividing numbers in the range [left, right].
 */

/**
 * @param {number} left
 * @param {number} right
 * @return {number[]}
 */
 var selfDividingNumbers = function(left, right) {
    //console.log(left)
    //console.log(right)
    
    let listOfSD = new Array();
    let baseArray = new Array(right-left);
    
    let index = 0;
    
    //fill a base array
    for (let i = left; i <= right; i ++) {
        if(index == 0) {
            baseArray[index] = left;
        }
        else {
            baseArray[index] = left + index;
        }
        index += 1;
    }
    
    let count = 0;
    //console.log('base array est : ', baseArray)
    
    let i = 0;

    //check list of Self Dividing Number in array and put them in array listOfSD
    for(i = 0; i < baseArray.length; i ++) {
        let digits = baseArray[i].toString().split('').map(Number)
        //console.log("digits : ", digits)

        count = 0;
        for(let j = 0; j < digits.length; j++) {
            let numOfDigits = digits.length
            
            //console.log('digits.length : ', digits.length)

            if(baseArray[i] % digits[j] !== 0) {
                count = 0;
                //console.log("reset");

            } else if(baseArray[i] % digits[j] === 0) {
                count += 1;
                //console.log('count : ', count)
                if(count == numOfDigits) {
                    //console.log("push dans la liste de : ", baseArray[i])
                    listOfSD.push(baseArray[i]);
                    //count = 0;
                }
            }

        }
    }
    return listOfSD
};
