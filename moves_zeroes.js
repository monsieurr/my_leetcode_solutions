/**
 * https://leetcode.com/problems/move-zeroes/
 * [283] Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 * Note that you must do this in-place without making a copy of the array.

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
 let moveZeroes = function(nums) {
    console.log(nums)
    
    let i = 0;
    let nonzero = [];
    let numsZeroes = 0;
    
    
    for(i; i < nums.length; i++) {
        console.log(nums[i])
        if(nums[i] === 0) {
            numsZeroes += 1;
        }
    }
    
    for(i = 0; i < nums.length; i++) {
        if(nums[i] != 0) {
            nonzero.push(nums[i])
        }
    }
    console.log(nonzero);
    
    while(numsZeroes--) {
        nonzero.push(0);
    }
    
    console.log(nonzero)
    
    for(i = 0; i < nums.length; i++) {
        nums[i] = nonzero[i];
    }
    
    return nums
}
