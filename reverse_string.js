/**
 * https://leetcode.com/problems/reverse-string/
 * [344] Write a function that reverses a string. The input string is given as an array of characters s.
 */

/**
 * @param {character[]} s
 * @return {void} Do not return anything, modify s in-place instead.
 */
 let reverseString = function(s) {
    // lazy solution, using reverse() method from Array.prototype
    s = s.reverse();
    console.log('after reverse : ', s);
    
    return s;
};

/**
 * Another way I would do it is (removing the not creating a new array condition) : 
 * 1. creating a new array
 * 2. iterating on s starting from the end of the loop and going back (i--)
 * 3. pushing each s[i] into the new array
 */

 /*let reverseString = function(s) {
    let newArray = []
    
    for(let i = s.length - 1; i >= 0; i--) {
        newArray.push(s[i]);
    }
    
    console.log(newArray);
    
    return newArray;
}

result = document.querySelector('.result');

let s = ["h","e","l","l","o"];

result.innerHTML = reverseString(s);
