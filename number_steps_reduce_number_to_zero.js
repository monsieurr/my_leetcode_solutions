/**
 * https://leetcode.com/problems/number-of-steps-to-reduce-a-number-to-zero/
 * [1342] Given an integer num, return the number of steps to reduce it to zero.
 * In one step, if the current number is even, you have to divide it by 2, otherwise, you have to subtract 1 from it.
 */

/**
 * @param {number} num
 * @return {number}
 */
 var numberOfSteps = function(num) {
    console.log(num)
    
    let nbOfSteps = 0;
    
    while(num > 0) {
        if(num % 2 === 0) {
            num = num / 2;
            nbOfSteps += 1;
        }
        else if(num % 2 !== 0) {
            num = num - 1;
            nbOfSteps += 1;
        }
    }
    
    
    return nbOfSteps;
};
