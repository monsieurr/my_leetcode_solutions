/*
* [1832] https://leetcode.com/problems/check-if-the-sentence-is-pangram/
* A pangram is a sentence where every letter of the English alphabet appears at least once.
* Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.
*
*/

/**
 * @param {string} sentence
 * @return {boolean}
 */
var checkIfPangram = function(sentence) {
    let isPengram = false;
    let alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    let count = 0;
    
    console.log('the sentence is : ', sentence);
    console.log('alpabet : ', alphabet);
    
    console.log(sentence.split(''))

    if(sentence.split('').length < 26) {
        return false;
    }
    
    sentenceArr = sentence.split('')
    
    for(let i = 0; i < sentence.split('').length; i++) {
        for(let j = 0; j < alphabet.length; j++) {
            if(sentenceArr[i] === alphabet[j]) {
                count += 1;
                
                alphabet = alphabet.filter(item => item !== alphabet[j])
                
                console.log(alphabet)
                
                console.log('count : ', count);
            }
        }
    }
    
    if(count === 26) {
        isPengram = true;
    }
    
    return isPengram;
};
