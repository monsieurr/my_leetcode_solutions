/**
 * https://leetcode.com/problems/count-of-matches-in-tournament/
 * [1688] You are given an integer n, the number of teams in a tournament that has strange rules:
 * If the current number of teams is even, each team gets paired with another team. A total of n / 2 matches are played, 
 * and n / 2 teams advance to the next round.
 * If the current number of teams is odd, one team randomly advances in the tournament, and the rest gets paired. 
 * A total of (n - 1) / 2 matches are played, and (n - 1) / 2 + 1 teams advance to the next round.
 * Return the number of matches played in the tournament until a winner is decided.
 * 
 */

/**
 * @param {number} n
 * @return {number}
 */
 var numberOfMatches = function(n) {
    console.log(n);
    let numberOfMatches = 0;
    
    let teamAdvance;
    let matchPlayes = 10;
    
    while(matchPlayes >= 1) {
        if(n % 2 === 0) {
            console.log("n is even : ", n)
            matchPlayes = (n) / 2;
            teamAdvance = n / 2;
            console.log('match playes : ', matchPlayes)
            console.log('team advance', teamAdvance)
            numberOfMatches += matchPlayes;
            n = teamAdvance;
        }
        else if(n % 2 !== 0) {
            console.log("n is odd : ", n)
            matchPlayes = (n - 1) / 2;
            teamAdvance = (n - 1) / 2 + 1;
            console.log('match playes : ', matchPlayes)
            console.log('team advance', teamAdvance)
            numberOfMatches += matchPlayes;
            n = teamAdvance;
        }
    }

    
    
    return numberOfMatches;
};
