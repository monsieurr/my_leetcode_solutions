/**
 * https://leetcode.com/problems/running-sum-of-1d-array/
 * [1480] Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
 * Return the running sum of nums.
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var runningSum = function(nums) {
    let runningSum = new Array(nums.length).fill(0)
    let counter = 0;
    
    for(let i = 0; i < nums.length; i ++) {
        runningSum[i] = counter + nums[i]
        counter += nums[i]
    }
    
    return runningSum
};