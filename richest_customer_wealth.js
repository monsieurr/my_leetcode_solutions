/* 
* https://leetcode.com/problems/richest-customer-wealth/
* [1672] You are given an m x n integer grid accounts where accounts[i][j] is the 
* amount of money the i​​​​​​​​​​​th​​​​ customer has in the j​​​​​​​​​​​th​​​​ bank. Return the wealth that the richest customer has.
* A customer's wealth is the amount of money they have in all their bank accounts. The richest customer is the customer that has the maximum wealth.
*/

/**
 * @param {number[][]} accountsO
 * @return {number}
 */
 var maximumWealth = function(accounts) {
    console.log("accounts length = ", accounts.length)
    let size;
    let count = new Array(accounts.length).fill(0);
    
    for(let i=0; i < accounts.length; i++) {
        for(let j=0; j < accounts[0].length; j++) {
            count[i] += (accounts[i][j])  
        }
    }
    var max_of_array = Math.max.apply(Math, count);
    console.log(max_of_array)
    
    return max_of_array
};