/**
 * https://leetcode.com/problems/check-if-two-string-arrays-are-equivalent/
 * [1662] Given two string arrays word1 and word2, return true if the two arrays represent the same string, and false otherwise.
 * A string is represented by an array if the array elements concatenated in order forms the string.
 */

/**
 * @param {string[]} word1
 * @param {string[]} word2
 * @return {boolean}
 */
 var arrayStringsAreEqual = function(word1, word2) {
    word1 = wordSum(word1);
    word2 = wordSum(word2);
    
    if(word1 === word2) {
       return true;
       }
    else {
        return false;
    }
};

function wordSum(word) {
    let wordSum;
    
    for(let i =0; i < word.length; i++) {
       wordSum += word[i]
    }
    
    return wordSum;
}
