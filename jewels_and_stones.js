/**
 * https://leetcode.com/problems/jewels-and-stones/
* [771] You're given strings jewels representing the types of stones that are jewels, 
* and stones representing the stones you have. Each character in stones is a type of stone you have. 
* You want to know how many of the stones you have are also jewels.
* Letters are case sensitive, so "a" is considered a different type of stone from "A".
 */


/**
 * @param {string} jewels
 * @param {string} stones
 * @return {number}
 */
 var numJewelsInStones = function(jewels, stones) {
    console.log(jewels.split(''))
    console.log(stones.split(''))
    
    let result = 0;
    jArray = jewels.split('')
    sArray = stones.split('')
    
    console.log(`taille de jewels array ${jArray.length}`)
    console.log(`taille de stones array ${sArray.length}`)

    
    for(let i=0; i < sArray.length; i++) {
        for(let j=0; j < jArray.length; j++) {
            if(sArray[i] == jArray[j]) {
                console.log('POUR S == J')
                console.log(`stones array ${sArray[i]}`)
                console.log(`jewels array ${jArray[i]}`)
                console.log("---")
                result += 1
            }
            else {
                console.log('POUR S != J')
                console.log(sArray[i])
                console.log(jArray[j])
            }
        }
    }
    
    return result;
};