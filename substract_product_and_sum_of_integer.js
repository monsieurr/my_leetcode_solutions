/**
 * https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/
 * [1281] Given an integer number n, return the difference between the product of its digits and the sum of its digits.
 */

/**
 * @param {number} n
 * @return {number}
 */
 var subtractProductAndSum = function(n) {
    let nums = [...n+''].map(e=>+e)
    console.log(nums)
    
    let product = 1;
    let sum = 0;
    
    for(i=0; i < nums.length; i++) {
        product *= nums[i]
        sum += nums[i]        
    }

    return product - sum
};