/**
 * https://leetcode.com/problems/two-sum/
* [1] Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
* You may assume that each input would have exactly one solution, and you may not use the same element twice.
* You can return the answer in any order.
 */


/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
 var twoSum = function(nums, target) {
    console.log(nums)
    console.log(target)
    
    let result = new Array(1).fill(0)
    console.log(result)
    console.log(`empty result array ${result}`)
    
    let okResult = 0
    
    for(let i = 0;i < nums.length; i++) {
        for(let j = 0; j < nums.length; j++) {
            if(i != j) {
                if (nums[i] + nums[j] == target) {
                    console.log(`first indice ${i} with element ${nums[i]}`)
                    console.log(`second indice ${j} with element ${nums[j]}`)

                    result[0] = i;
                    result[1] = j;

                    okResult = 1
                }
            }
            if(okResult == 1) {
                console.log("breaking")
                break;
            }
        }
    }
    
    console.log(`result ${result}`)
    return result;
};