/**
 * https://leetcode.com/problems/reverse-integer/
 * [7] Given a signed 32-bit integer x, return x with its digits reversed. 
 * If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
 * Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
 * 
 */

/**
 * @param {number} x
 * @return {number}
 */

 function reversedNum(num) {
    return (
      parseFloat(
        num
          .toString()
          .split('')
          .reverse()
          .join('')
      ) * Math.sign(num)
    )                 
  }
  
  var reverse = function(x) {
      
      x = reversedNum(x)
      
      if(x <  Math.pow(-2, 31) || x > Math.pow(2, 31) - 1) {
         return 0;
      }
  
      
      return x;
  };