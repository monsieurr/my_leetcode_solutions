/**
 * https://leetcode.com/problems/number-of-steps-to-reduce-a-number-to-zero/
 * [561] Given an integer array nums of 2n integers, group these integers into n pairs 
 * (a1, b1), (a2, b2), ..., (an, bn) such that the sum of min(ai, bi) for all i is maximized. Return the maximized sum.
 */


/**
 * @param {number[]} nums
 * @return {number}
 */
 var arrayPairSum = function(nums) {
    let maxPossibleSum = 0;
    
    // sort the array
    //nums = nums.sort()
    nums = nums.sort(function(a,b) { return a - b; });
    console.log('sorted array : ', nums)
    console.log('nums lengths : ', nums.length)
    
    // makes pairs
    // iterate by +2 into the array to form pairs into a new array
    let pairs = []
    let count = 0;
    for(let i = 0; i < nums.length; i+=2) {
        pairs[count] = Math.min(nums[i], nums[i+1])
        count += 1;
    }
    
    // make the min sum of each pair
    console.log('pairs min : ', pairs)
    for(let i = 0; i < pairs.length; i++) {
        maxPossibleSum += pairs[i]
    }
    
    // return the result as the max possible sum
    return maxPossibleSum;
};
