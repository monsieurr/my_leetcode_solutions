/**
 * https://leetcode.com/problems/squares-of-a-sorted-array/
 * [977] Given an integer array nums sorted in non-decreasing order, 
 * return an array of the squares of each number sorted in non-decreasing order.
 */


/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function(nums) {
    //console.log('1. nums not sorted : ', nums);
    
    for(let i = 0; i < nums.length; i++) {
        nums[i] *= nums[i]; 
    }
    
    //console.log('2. nums squared not sorted : ', nums);
           
    nums = nums.sort(function (a, b) {  return a - b;  });
    
    //console.log('3. nums squared sorted : ', nums);
    
    return nums;

};
