/*
* https://leetcode.com/problems/two-sum/
* [1] Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
* You may assume that each input would have exactly one solution, and you may not use the same element twice.
* You can return the answer in any order.
*/
class Solution {
    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function twoSum($nums, $target) {
        $size = count($nums);
        
        for($i = 0; $i < $size; $i++) {
            for($j = 0; $j < $size; $j++) {
                if($i != $j && $nums[$i] + $nums[$j] == $target) {
                    return array($i, $j);
                }
            }
        }
        return $nums;
    }
}
