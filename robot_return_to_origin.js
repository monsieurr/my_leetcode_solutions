/**
 * https://leetcode.com/problems/robot-return-to-origin/
 * [657] There is a robot starting at the position (0, 0), the origin, on a 2D plane. Given a sequence of its moves, judge if this robot ends up at (0, 0) after it completes its moves.
 * You are given a string moves that represents the move sequence of the robot where moves[i] represents its ith move. 
 * Valid moves are 'R' (right), 'L' (left), 'U' (up), and 'D' (down).
 * Return true if the robot returns to the origin after it finishes all of its moves, or false otherwise.
 */

/**
 * @param {string} moves
 * @return {boolean}
 */
 var judgeCircle = function(moves) {
    let hasReturnedToOrigin = false;
    let arrGrid = [0, 0];
    
    arrMoves = moves.split('');
    //console.log(arrMoves);
    
    
        for(let i = 0; i < arrMoves.length; i++) {
            if(arrMoves[i] === 'L') {
                //console.log('LEFT - arrMoves[i] : ', arrMoves[i])
                arrGrid[0] -= 1;
            }
            else if(arrMoves[i] === 'R') {
                //console.log('RIGT - arrMoves[i] : ', arrMoves[i])
                arrGrid[0] += 1;
            }
            else if(arrMoves[i] === 'U') {
                //console.log('UP - arrMoves[i] : ', arrMoves[i])
                arrGrid[1] += 1;
            }
            else {
                //console.log('DOWN - arrMoves[i] : ', arrMoves[i])
                arrGrid[1] -= 1;
            }  
        }
        
    //console.log(arrGrid[0], arrGrid[1])
    
    if(arrGrid[0] === 0 && arrGrid[1] === 0) {
        hasReturnedToOrigin = true;
    }

    return hasReturnedToOrigin;
};
