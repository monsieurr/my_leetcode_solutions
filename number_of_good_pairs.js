/**
 * https://leetcode.com/problems/number-of-good-pairs/
 * [1512] Given an array of integers nums.
 * A pair (i,j) is called good if nums[i] == nums[j] and i < j.
 * Return the number of good pairs.
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
 var numIdenticalPairs = function(nums) {
    let goodPairs = 0;
    
    console.log(nums)
    
    for(let i = 0; i < nums.length; i++) {
        for(let j = 0; j < nums.length; j++) {
            if(nums[i] == nums[j] && i < j) {
                goodPairs += 1;
            }       
        }
    }
    return goodPairs
};