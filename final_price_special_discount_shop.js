/**
 * https://leetcode.com/problems/final-prices-with-a-special-discount-in-a-shop/
 * [1475] Given the array prices where prices[i] is the price of the ith item in a shop. 
 * There is a special discount for items in the shop, if you buy the ith item, then you will receive a discount equivalent 
 * to prices[j] where j is the minimum index such that j > i and prices[j] <= prices[i], otherwise, 
 * you will not receive any discount at all.
 * Return an array where the ith element is the final price you will pay for the ith item of the shop considering 
 * the special discount.
 */

/**
 * @param {number[]} prices
 * @return {number[]}
 */
 var finalPrices = function(prices) {
    //console.log(prices)
    
    let finalPriceIthItem = [];
    let okDiscount = false;
    //prices.map(e => console.log(e))
    
    for(let i = 0; i < prices.length; i++) {
        for(let j = 0; j < prices.length; j++) {
            //apply the discount
            if(j > i && prices[j] <= prices[i] && !okDiscount) {
                /*console.log('-DISCOUNT-')
                console.log(`index i : ${i} and index j :  ${j}`)
                console.log('price i : ', prices[i])
                console.log('price j : ', prices[j])
                console.log('okDiscount ?', okDiscount)
                console.log('-----------')*/
                finalPriceIthItem.push(prices[i] - prices[j])
                okDiscount = true;
            } /*else {
                /*console.log('NO DISC')
                console.log(`index i : ${i} and index j :  ${j}`)
                console.log('price i : ', prices[i])
                console.log('price j : ', prices[j])
                console.log('okDiscount ?', okDiscount)
                console.log('-----------') 
            }*/
        }
        //console.log('///////////////')
        if(okDiscount === false) {
            finalPriceIthItem.push(prices[i])
        } else {
            //console.log('Price OK, Reset DISCOUNT')
            okDiscount = false;
        }

    }

    return finalPriceIthItem;
};
