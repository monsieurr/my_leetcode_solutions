/**
 * https://leetcode.com/problems/palindrome-number/
 * [9] Given an integer x, return true if x is palindrome integer.
 * An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.
 */


function reverse_a_number(n)
{
    /*we split the number into individual parts, reverse it and then join all the parts*/
	n = n + "";
	return n.split("").reverse().join("");
}

/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
   console.log(x)
    
    let xl = x;
    let xr = reverse_a_number(x)
    
    console.log('xl : ', xl)
    console.log('xr : ', xr)
    
    if(xl == xr) {
        return true;
    }
    else {
        return false
    }
    
};